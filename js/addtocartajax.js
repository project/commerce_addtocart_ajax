/**
 * @file
 * JavaScript behaviors for Commerce add to cart ajax.
 */

(function ($, Drupal) {
  /**
   * Remove views/ajax url for commerce addtocart ajax submissions.
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attaches the behavior for commerce addtocart ajax.
   */
  Drupal.behaviors.commerceAddToCartAjax = {
    attach: (context) => {
      // Add submit handler to form.beforeSend.
      // Update Drupal.Ajax.prototype.beforeSend only once.
      if (
        typeof Drupal.Ajax !== 'undefined' &&
        typeof Drupal.Ajax.prototype.beforeSubmitCommerceAddToCartAjax ===
          'undefined'
      ) {
        Drupal.Ajax.prototype.beforeSubmitCommerceAddToCartAjax =
          Drupal.Ajax.prototype.beforeSubmit;

        Drupal.Ajax.prototype.beforeSubmit = function (...args) {
          const [formValues, elementSettings, options] = args;

          if (
            this.callback === 'commerce_addtocart_ajax_ajax_validate' &&
            options.url.indexOf('/views/ajax') === 0
          ) {
            // Remove /views/ajax from url.
            options.url = options.url.replace('/views/ajax', '');
          }
          return this.beforeSubmitCommerceAddToCartAjax(...args);
        };
      }
    },
  };
})(jQuery, Drupal);
